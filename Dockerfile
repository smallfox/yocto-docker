FROM ubuntu:22.04

RUN apt-get -y update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install \
  gawk \
  file \
  wget \
  git \
  diffstat \
  unzip \
  texinfo \
  gcc \
  build-essential \
  chrpath \
  socat \
  cpio \
  python3 \
  python3-pip \
  python3-pexpect \
  xz-utils \
  debianutils \
  iputils-ping \
  python3-git \
  python3-jinja2 \
  libegl1-mesa \
  libsdl1.2-dev \
  pylint \
  xterm \
  python3-subunit \
  mesa-common-dev \
  zstd \
  liblz4-tool \
  locales

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen

RUN git clone https://git.yoctoproject.org/poky
RUN useradd -d /poky -s /bin/bash -m -u 6000 poky && chown -R poky: /poky

